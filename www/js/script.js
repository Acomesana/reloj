'use strict'
let mireloj = document.getElementById("reloj");
let p = document.createElement('p');
mireloj.appendChild(p);


function actual() {
    let fecha = new Date();//Actualizamos fecha
    let hora = fecha.getHours();// Hora actual
    let minuto = fecha.getMinutes();//minuto actual
    let segundo = fecha.getSeconds();// segundo actual


    if (hora < 10) {
        hora = "0" + hora;
    }
    if (minuto < 10) {
        minuto = "0" + minuto;
    }
    if (segundo < 10) {
        segundo = "0" + segundo;
    }

    //devolver datos:
    let mireloj = hora + ": " + minuto + ": " + segundo;
    return mireloj;
}

function actualizar() {
    setTimeout(() => {
        let mihora = actual();
        p.innerHTML = mihora;
        actualizar()
    }, 1000);

}
actualizar()


